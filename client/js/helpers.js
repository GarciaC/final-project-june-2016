"use strict";

Template.posts.helpers({
  postList: function () {
    return Collections.Appwall.find({}, {
      sort: {
        createdAt: -1
      }
    });
  },

  updateMasonry: function () {
    $('.grid').imagesLoaded().done(function () {
      $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true,
        gutter: 10
      });
    });
  },

  imageIs: function (imageId) {
    var image = Collections.Images.find({
      _id: imageId
    }).fetch();
    return image[0].url();
  }
});

Accounts.ui.config({
  passwordSignupFields: "USERNAME_ONLY"
});


$(document).ready(function () {
  $('.parallax').parallax();
});