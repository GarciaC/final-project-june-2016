"use strict";

Template.form.events({
  'submit .add-new-post': function (event) {
    event.preventDefault();

    var postImage = event.currentTarget.children[3].children[0].children[1].files[0],
      message = event.currentTarget.children[0].firstElementChild.value,
      price = event.currentTarget.children[1].firstElementChild.value,
      description = event.currentTarget.children[2].firstElementChild.value;

    console.log(postImage);
    console.log(message);

    if (message === "") {
      Materialize.toast('Please type in something', 4000);
      return false;
    }
    if (price === "") {
      Materialize.toast('Please type in something', 4000);
      return false;
    }
    if (description === "") {
      Materialize.toast('Please type in something', 4000);
      return false;
    }
    if (postImage === undefined) {
      Materialize.toast('You need to insert an image', 4000);
      return false;
    }

    Collections.Images.insert(postImage, function (error, fileObject) {
      if (error) {
        alert("There was an error!");
      } else {
        Collections.Appwall.insert({
          message: message,
          price: price,
          description: description,
          createdAt: new Date(),
          owner: Meteor.userId(),
          username: Meteor.user().username,
          imageId: fileObject._id
        });
      }
      $('.grid').masonry('reloadItems');
    });
  }
});

Template.posts.events({
  'click .delete-task': function (event) {
    if (this.username === Meteor.user().username) {
      Collections.Appwall.remove({
        _id: this._id
      });
    } else {
      Materialize.toast('This is not your post', 4000);
      return false;
    }
  }
});

Template.sorts.events({
  'click .sort-task': function (event) {
    return Collections.Appwall.find({}, {
      sort: {
        createdAt: 1
      }
    });
  }
});