Collections = {};

var imageStore = new FS.Store.GridFS("images");

Collections.Images = new FS.Collection("images", {
  stores: [imageStore]
});

Collections.Appwall = new Mongo.Collection("appwall");

UsersIndex = new EasySearch.Index({
  collection: Meteor.users,
  fields: ['username'],
  engine: new EasySearch.Minimongo()
});